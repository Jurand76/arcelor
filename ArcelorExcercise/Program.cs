﻿using System;

namespace Arcelor
{
    class Program
    {

        public static string Counter(string input)
        {
            // check if input string is not empty
            if (String.IsNullOrEmpty(input))
            {
                return "Empty string as input";
            }
            
            // list of tuples
            List<(int, char)> output = new List<(int, char)> ();

            int counter = 1;

            for (int i = 1; i < input.Length; i++)
            {
                char current = input[i];
                char previous = input[i - 1];

                // check if previous char was the same like current
                if (current == previous)
                {
                    counter += 1;
                }
                else
                {
                    output.Add((counter, previous));
                    counter = 1;
                }

                // add counter status when index is at last char
                if (i == input.Length - 1)
                {
                    output.Add((counter, current));
                }
            }

            // generate output string from list of tuples
            string result = "";

            for (int i = 0; i < output.Count; i++)
            {
                result += $"({output[i].Item1}, {output[i].Item2})";
                if (i < output.Count - 1)
                {
                    result += " ";
                }
            }

            return result;
        }

        static void Main()
        {
            string input = "1222311";
            string output = Counter(input);

            Console.WriteLine($"Input string: {input}");
            Console.WriteLine($"Output string: {output}");

            input = "122333444455555666666";
            output = Counter(input);

            Console.WriteLine($"Input string: {input}");
            Console.WriteLine($"Output string: {output}");

            input = "alamakotaakotmaczterynogi";
            output = Counter(input);

            Console.WriteLine($"Input string: {input}");
            Console.WriteLine($"Output string: {output}");

            input = "toomaaaanyzzzzzIIIUUUUU";
            output = Counter(input);

            Console.WriteLine($"Input string: {input}");
            Console.WriteLine($"Output string: {output}");

            input = "";
            output = Counter(input);

            Console.WriteLine($"Input string: {input}");
            Console.WriteLine($"Output string: {output}");
        }
    }
}
